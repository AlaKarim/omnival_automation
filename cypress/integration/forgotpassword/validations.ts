import '../../support/commands'
import '../../../node_modules/cypress-plugin-tab/src/index'
import { internet } from 'faker'

describe('Validations on ForgotPassword Page', () => {
  before(() => { 
     cy.redirectForgot()
    })

    it('assert login page labels', () => {
        cy.get('[data-cy=client]').should('have.text', 'Client')     
        cy.get('[data-cy=h1]').should('have.text', 'Forgot Password')  
        cy.get('[data-cy=h2]').should('have.text', "Enter your username and we'll send you an email with instructions to reset your password.")  
        cy.get('[data-cy=h3]').should('have.text', 'Don’t have an account? Sign Up')
        cy.get('[data-cy=backLink]').should('have.text', ' Back')
        cy.get('[data-cy=username]').should('have.text', 'Username')
    })

    it('Username field validations', () => {
        const randomUsername = internet.userName()
        cy.get('[formcontrolname=userLogin]').focus().blur()
        cy.get('[data-cy=username-error]').should('contain', 'Username is required')
        cy.get('[formcontrolname=userLogin]').type('!@#$%^').focus().blur()
        cy.get('[data-cy=username-error]').should('contain', 'Username contains invalid characters')
        cy.get('[formcontrolname=userLogin]').clear().type(randomUsername).focus().blur()
        cy.get('[data-cy=username-error]').should('contain', ' Incorrect username. Please try again ')
      })

   after(() => { 
        cy.screenShot()
       })

})