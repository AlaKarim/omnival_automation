import '../../support/commands'
import '../../../node_modules/cypress-plugin-tab/src/index'

describe('Success ForogtPassword', () => {
    before(() => { 
       cy.redirectForgot()
      })
  
      it('send forgotpassword email', () => {
          cy.get('[formcontrolname=userLogin]').type("tokyo").focus().blur()
          cy.get('[data-cy=submitbtn]').click()
          cy.go('forward')
          cy.get('.mat-simple-snackbar').should('have.text', 'Email Sent Successfully')
        
      })
      
     after(() => { 
          cy.screenShot()
         })
  
  })