import '../../support/commands'
import 'cypress-plugin-tab/src'
import { internet } from 'faker'
import { name } from 'faker'
import { company } from 'faker'
import { phone } from 'faker'


describe('successful sign up', () => {
    it('should redirect to sign up page', () => {
     cy.redirectSignup()
  })

  it('Step1', () => {
    const randomFirstName = name.firstName()
    const randomLastName = name.lastName()
    const randomEmail = internet.email()
    const randomUsername = internet.userName()
    const randomPassword = "Testing@123"
    cy.get('[formcontrolname=firstName]').type(randomFirstName)
    cy.get('[formcontrolname=lastName]').type(randomLastName)
    cy.get('[formcontrolname=email]').type(randomEmail)
    cy.get('[formcontrolname=userLogin]').type(randomUsername)
    cy.get('[formcontrolname=password]').type(randomPassword)
    cy.get('[formcontrolname=clientTypeId]').click()
    cy.get('.mat-option-text').contains('Financer').click()
    cy.get('[data-cy=submitbtn]').click()
   })

   it('Step2', () => {
    const randomCompanyName = company.companyName()
    const randomPhoneNo = phone.phoneNumberFormat()
    cy.get('[formcontrolname=companyName]').type(randomCompanyName)
    cy.get('[formcontrolname=contactPhone]').type(randomPhoneNo)
    cy.get('[formcontrolname=averageLoanVolumePerMonth]').click()
    cy.get('.mat-option-text').contains(' 500 - 999 ').click()
    cy.get('[data-cy=completeBtn]').click()
    cy.wait(2000)
    cy.get('[data-cy=successMessage]').should('have.text', "You've successfully registered to OmniVal.") 
   })

   after(() => { 
      cy.screenShot()
     })
})