import '../../support/commands'
import 'cypress-plugin-tab/src'

describe('Validations on Sign Up- Step2', () => {
  before(() => { 
     cy.redirectSignup()
     cy.signUp()
  })

  // it('verify API calls ', () => {
  //   cy.request('http://localhost:5000/api/clients/client-types').should((response) => {
  //     expect(response.status).to.eq(200)
  //     expect(response).to.have.property('headers')
  //     expect(response).to.have.property('duration')
  //   })

  //     cy.request('http://localhost:5000/api/products/product-types').should((response) => {
  //     expect(response.status).to.eq(200)
  //     expect(response).to.have.property('headers')
  //     expect(response).to.have.property('duration')
  //   })

  //     cy.request('http://localhost:5000/api/average-loan-volume-ranges').should((response) => {
  //     expect(response.status).to.eq(200)
  //     expect(response).to.have.property('headers')
  //     expect(response).to.have.property('duration')

  //   })
  // })

  it('assert sign up step2 labels', () => {
      cy.get('[data-cy=Client]').should('have.text', 'Client')     
      cy.get('[data-cy=h1]').should('have.text', 'Sign Up to OmniVal')  
      cy.get('[data-cy=h2]').should('have.text', 'Enter your details below')  
      cy.get('[data-cy=h3]').should('have.text', 'Already have an account? Login')
      cy.get('[data-cy=termsStep2]').should('have.text', 'Terms of Services')
      cy.get('[data-cy=policyStep2]').should('have.text', 'Privacy Policy.')
      cy.get('[data-cy=CN]').should('have.text', 'Company Name')
      cy.get('[data-cy=CP]').should('have.text', 'Contact Phone')
      cy.get('[data-cy=AL]').should('have.text', 'Average Loan Value Per Month')
      cy.get('[data-cy=AD]').should('have.text', 'Find your Address')
  })

  it('verifies login link', () => {
    cy.get('[data-cy=loginlink]').should('be.visible')
    
  })

  it('verifies term of services link', () => {
    cy.get('[data-cy=termsStep2]').should('be.visible')
    
  })

  it('verifies privacy policy link', () => {
    cy.get('[data-cy=policyStep2]').should('be.visible')
  })

  it('companyName field validations', () => {
    cy.get('[formcontrolname=companyName]').focus().blur()
    cy.get('[data-cy=companyName-error]').should('contain', 'Company name is required')
 
  })

  it('phoneNumber field validations', () => {
    cy.get('[formcontrolname=contactPhone]').focus().blur()
    cy.get('[data-cy="phoneNum-error"]').should('contain', 'Contact Phone is required')
    cy.get('[formcontrolname=contactPhone]').clear().type('667').blur()
    cy.get('[data-cy="phoneNum-error"]').should('contain', "Contact Phone must be in correct format (XXX-XXX-XXXX)")
  })

  it('AverageLoanValuePerMonth is required', () => {
    cy.get('[formcontrolname=averageLoanVolumePerMonth]').focus().blur()
    cy.get('[data-cy=avgLoan-error]').should('contain', 'Average Loan is not selected')
  })

  it('Should use tab keyword', () => {
    cy.get('[formcontrolname=companyName]').tab().tab().tab().tab().tab().tab().tab().tab().tab().tab().type('{enter}') 
  })

  after(() => { 
    cy.screenShot()
   })
})