import '../../support/commands'
import 'cypress-plugin-tab/src'

describe('Validations on Sign up- Step1', () => {
  before(() => { 
     cy.redirectSignup()
  })

  // it('verify API calls ', () => {
  //   cy.request('http://localhost:5000/api/clients/client-types').should((response) => {
  //     expect(response.status).to.eq(200)
  //     expect(response).to.have.property('headers')
  //     expect(response).to.have.property('duration')
  //   })

  //     cy.request('http://localhost:5000/api/products/product-types').should((response) => {
  //     expect(response.status).to.eq(200)
  //     expect(response).to.have.property('headers')
  //     expect(response).to.have.property('duration')
  //   })

  //     cy.request('http://localhost:5000/api/average-loan-volume-ranges').should((response) => {
  //     expect(response.status).to.eq(200)
  //     expect(response).to.have.property('headers')
  //     expect(response).to.have.property('duration')

  //   })
  // })

  it('assert signup step1 labels', () => {
      cy.get('[data-cy=Client]').should('have.text', 'Client')     
      cy.get('[data-cy=h1]').should('have.text', 'Sign Up to OmniVal')  
      cy.get('[data-cy=h2]').should('have.text', 'Enter your details below')  
      cy.get('[data-cy=h3]').should('have.text', 'Already have an account? Login')
      cy.get('[data-cy=termsStep1]').should('have.text', 'Terms of Services')
      cy.get('[data-cy=policyStep1]').should('have.text', 'Privacy Policy.')
      cy.get('[data-cy=FN]').should('have.text', 'First Name')
      cy.get('[data-cy=LN]').should('have.text', 'Last Name')
      cy.get('[data-cy=CT]').should('have.text', 'Client Type')
      cy.get('[data-cy=UN]').should('have.text', 'Username')
      cy.get('[data-cy=PW]').should('have.text', 'Password')
      cy.get('[data-cy=EM]').should('have.text', 'Email')
  })

  it('verifies login link', () => {
    cy.get('[data-cy=loginlink]').should('be.visible')
    
  })

  it('verifies term of services link', () => {
    cy.get('[data-cy=termsStep1]').should('be.visible')
    
  })

  it('verifies privacy policy link', () => {
    cy.get('[data-cy=policyStep1]').should('be.visible')
  })

  it('firstname field validations', () => {
    cy.get('[formcontrolname=firstName]').focus().blur()
    cy.get('[data-cy=firstname-error]').should('contain', 'First name is required')
    cy.get('[formcontrolname=firstName]').clear().type('@#$%^&').blur()
    cy.get('[data-cy=firstname-error]').should('contain', "First name can include numbers, period ( . ), hyphen ( - ) and apostrophe ( ' )")

  })

  it('lastname field validations', () => {
    cy.get('[formcontrolname=lastName]').focus().blur()
    cy.get('[data-cy=lastname-error]').should('contain', 'Last name is required')
    cy.get('[formcontrolname=lastName]').clear().type('@#$%^&').blur()
    cy.get('[data-cy=lastname-error]').should('contain', "Last name can include numbers, period ( . ), hyphen ( - ) and apostrophe ( ' )")
  })

  it('Client Type is required', () => {
    cy.get('[formcontrolname=clientTypeId]').focus().blur()
    cy.get('[data-cy=ctype-error]').should('contain', 'Client Type is not selected')
  })

  it('Email field validations', () => {
    cy.get('[formcontrolname=email]').focus().blur()
    cy.get('[data-cy=email-error]').should('contain', 'Email is required')
    cy.get('[formcontrolname=email]').type('test.com')
    cy.get('[data-cy=email-error]').should('contain', ' Email is invalid')
  })

  it('Username field validations', () => {
    cy.get('[formcontrolname=userLogin]').focus().blur()
    cy.get('[data-cy=username-error]').should('contain', 'Username is required')
    cy.get('[formcontrolname=userLogin]').type('test').focus().blur()
    cy.get('[data-cy=username-error]').should('contain', 'Username already exist') 
    cy.get('[formcontrolname=userLogin]').type('!@#$%^').focus().blur()
    cy.get('[data-cy=username-error]').should('contain', 'Username contains invalid characters')
  })

  it('Password field validations', () => {
    cy.get('[formcontrolname=password]').focus().blur()
    cy.get('[data-cy=password-error]').should('contain', 'Password is required')
    cy.get('[formcontrolname=password]').type('test')
    cy.get('[data-cy=password-error]').should('contain', ' Password must contains at least 8 characters one numeric one uppercase one lowercase and one special character ')
  })

  it('show password', () => {
    cy.get('[data-cy=hideIcon]').click()
  })

  it('Should use tab keyword', () => {
    cy.get('[formcontrolname=firstName]').tab().tab().tab().tab().tab().type('{enter}') 
  })

  after(() => { 
    cy.screenShot()
   })
})