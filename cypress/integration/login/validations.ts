import '../../support/commands'
import '../../../node_modules/cypress-plugin-tab/src/index'

describe('Validations on Login Page', () => {
  before(() => { 
     cy.redirectLogin()
    })

  it('assert login page labels', () => {
      cy.get('[data-cy=Client]').should('have.text', 'Client')     
      cy.get('[data-cy=h1]').should('have.text', 'Sign in to OmniVal')  
      cy.get('[data-cy=h2]').should('have.text', 'Enter your details below')  
      cy.get('[data-cy=h3]').should('have.text', 'Don’t have an account? Sign Up')
      cy.get('[data-cy=forgotlink]').should('have.text', 'Forgot your password?')
      cy.get('[data-cy=username]').should('have.text', 'Username')
      cy.get('[data-cy=password]').should('have.text', 'Password')
  })

  it('verifies sign up link', () => {
    cy.get('[data-cy=signuplink]').should('be.visible').click()
    cy.go('back')
  })

  it('verifies forgot password link', () => {
    cy.get('[data-cy=forgotlink]').should('be.visible').click()
    cy.go('back')
  })

  it('username is required', () => {
    cy.get('[formcontrolname=username]').click() 
    cy.get('[formcontrolname=username]').focus().blur()
    cy.get('[data-cy=username-error]').should('contain', 'Username is required')
  })

  it('password is required', () => {
    cy.get('[formcontrolname=password]').click() 
    cy.get('[formcontrolname=password]').focus().blur()
    cy.get('[data-cy=password-error]').should('contain', 'Password is required')
  })

  it('show password', () => {
    cy.get('[formcontrolname=password]').type('test')
    cy.get('[data-cy=hideIcon]').click()
  })

  it('should use tab keyword', () => {
    cy.get('[formcontrolname=username]').type('cbot').tab().type('Testing123').tab().type('{enter}') 
  })

  it('incorrect username/password', () => {
    cy.get('.mat-simple-snackbar').should('have.text', 'Incorrect username or password. Please try again')
  })

  after(() => { 
    cy.screenShot()
   })
})