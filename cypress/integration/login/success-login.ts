import '../../support/commands'

describe('Success Login', () => {
  it('should redirect to login page', () => {
   cy.redirectLogin() 
  })

  it('should login', () => {
   cy.login()
  })

  // it('verify login API call ', () => {
  //   cy.request('/api/accounts/clients/signin').should((response) => {
  //     expect(response.status).to.eq(200)
  //     expect(response).to.have.property('headers')
  //     expect(response).to.have.property('duration')
  //   })
  // })
  
  after(() => { 
    cy.screenShot()
   })

})
