// ***********************************************
// This example namespace declaration will help
// with Intellisense and code completion in your
// IDE or Text Editor.
// ***********************************************
// declare namespace Cypress {
//   interface Chainable<Subject = any> {
//     customCommand(param: any): typeof customCommand;
//   }
// }
//
// function customCommand(param: any): void {
//   console.warn(param);
// }
//
// NOTE: You can use it like so:
// Cypress.Commands.add('customCommand', customCommand);
//
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })


// ********* Login Method ********* //
Cypress.Commands.add('loginInfo', (username, password) => {
    cy.log('filling out username')
    cy.get('[formcontrolname=username]').type(username)
    cy.log('filling out password')
    cy.get('[formcontrolname=password]').type(password)
    cy.log('Signing in')
    cy.get('[data-cy=submit]').click()
  })


// ******** SignUp Method ********** //
Cypress.Commands.add('signUpInfo', (firstname, lastname, email, username, password) => {
    cy.get('[formcontrolname=firstName]').type(firstname)
    cy.get('[formcontrolname=lastName]').type(lastname)
    cy.get('[formcontrolname=email]').type(email)
    cy.get('[formcontrolname=userLogin]').type(username)
    cy.get('[formcontrolname=password]').type(password)

} )

// ********* Login Redirection Method ********* //
Cypress.Commands.add('redirectLogin', () => {
    cy.exec('npm cache clear --force')
    cy.visit('/account/login')
})

// ********* Signup Redirection Method ********* //
Cypress.Commands.add('redirectSignup', () => {
    cy.exec('npm cache clear --force')
    cy.visit('/account/signup')
})

// ********* ForogtPassword Redirection Method ********* //
Cypress.Commands.add('redirectForgot', () => {
    cy.exec('npm cache clear --force')
    cy.visit('/account/forgotpassword')
})

// ********* Login data ********* //
Cypress.Commands.add('login', () => {
    cy.loginInfo('cbot', 'Testing@123')
})

// ********* SignUp data ********* //
Cypress.Commands.add('signUp', () => {
    cy.signUpInfo('bla','test','akarim@spursol.com','nui4','Testing@123')
    cy.get('[formcontrolname=clientTypeId]').click()
    cy.get('.mat-option-text').contains('Financer').click()
    cy.get('[data-cy=submitbtn]').click()
})

// ********* Screenshot Method ********* //
Cypress.Commands.add('screenShot', () => {
    cy.screenshot()
})
  